python -m ensurepip --default-pip
pip install virtualenv
virtualenv venv
pip install -r requirements.txt

set FLASK_APP=src/app.py
set FLASK_ENV=development
set FLASK_DEBUG=1
set PYTHONPATH=./src/
python tests\test_csv2any.py
python tests\test_html2any.py
python tests\test_ms_office2any.py
python tests\test_pdf2any.py
