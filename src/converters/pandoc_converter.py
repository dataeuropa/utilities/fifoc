import pypandoc
from converters.converterMixin import converterMixin
import re

__ACCEPTED_PANDOC_FORMATS_STR = """biblatex,bibtex,commonmark,commonmark_x,creole,csljson,csv,
docbook,docx,dokuwiki,endnotexm,epub,fb2,gfm,haddock,html,ipynb,jats,jira,json,latex,man,markdown,
markdown_github,markdown_mmd,markdown_phpextra,markdown_strict,mediawiki,muse,native,
odt,opml,org,ris,rst,rtf,t2t,textile,tikiwiki,twiki,vimwiki"""

ACCEPTED_PANDOC_INPUT_FORMATS = {
    in_format.strip()
    for in_format in __ACCEPTED_PANDOC_FORMATS_STR.strip().split(",")
}

__ACCEPTED_PANDOC_OUTPUT_FORMATS_STR = """
asciidoc,asciidoctor,beamer,biblatex,bibtex,commonmark,commonmark_x,context,csljson,docbook,docbook4,
docbook5,docx,dokuwiki,dzslides,epub,epub2,epub3,fb2,gfm,haddock,html,html4,html5,icml,ipynb,jats,
jats_archiving,jats_articleauthoring,jats_publishing,jira,json,latex,man,markdown,markdown_github,
markdown_mmd,markdown_phpextra,markdown_strict,markua,mediawiki,ms,muse,native,odt,opendocument,
opml,org,pdf,plain,pptx,revealjs,rst,rtf,s5,slideous,slidy,tei,texinfo,textile,xwiki,zimwiki
"""
ACCEPTED_PANDOC_OUTPUT_FORMATS = {
    out_format.strip()
    for out_format in __ACCEPTED_PANDOC_OUTPUT_FORMATS_STR.strip().split(",")
}


class PandocConverter(converterMixin):

    def convert(self, infile, in_format, out_format, outfile):
        self._log.debug("convert try to convert file: %s " % str(infile))
        # try:
        converted = pypandoc.convert_file(str(infile), format=in_format,
                                          to=out_format, outputfile=str(outfile), extra_args=('--standalone','--wrap=none'))
        # except RuntimeError as e:
        #     self._log.debug("Exception")
        #     self._log.debug(e, exc_info=True)
        #     msg = e.args[0]
        #     m = re.search('((\/.*)*\w+[.]\w+)', msg)
        #     if m is None:
        #         raise
        #     else:  # If the error message contains the file name, replace the temp file name with the original file name
        #         # msg = msg.replace(m.group(0), f"{str(infile).split('/')[-1]}")
        #         if "Error at" in msg:
        #             raise RuntimeError("Error at" + msg.split("Error at")[-1])
        #         raise RuntimeError(":".join(msg.replace(m.group(0), f"{str(infile).split('/')[-1]}").split(':')[1:]))
            # raise FIFOCException(' '.join(groups), 400) from RuntimeError
        self._log.debug(">>>>>>>>>>>>>>>>>>> Convert file: %s done." % str(converted))
        self._log.info(f"{self.__class__.__name__}: Success!")
