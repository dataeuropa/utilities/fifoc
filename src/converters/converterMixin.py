import logging
from util.app_configuration import AppConfiguration


class converterMixin:
    def __init__(self):
        self._log = logging.getLogger(self.__class__.__name__)
        self._config = AppConfiguration()
