import csv
import json
import Csv2Xml
import pandas as pd
import chardet
import codecs

from converters.converterMixin import converterMixin

__ACCEPTED_CSV_FORMATS_STR = """csv"""

ACCEPTED_CSV_INPUT_FORMATS = {
    in_format.strip()
    for in_format in __ACCEPTED_CSV_FORMATS_STR.strip().split(",")
}

__ACCEPTED_CSV_OUTPUT_FORMATS_STR = """
xls, xlsx, json, xml
"""
ACCEPTED_CSV_OUTPUT_FORMATS = {
    out_format.strip()
    for out_format in __ACCEPTED_CSV_OUTPUT_FORMATS_STR.strip().split(",")
}


class CsvConverter(converterMixin):

    def convert(self, infile, out_format, outfile):
        self._log.debug("convert try to convert file: %s " % str(infile))
        # be sure that any csv file is utf-8 encoded
        self.convert_codec2utf8(infile)
        if out_format == "json":
            self.csv_to_json(infile, outfile)
        elif out_format == "xls":
            self.csv_to_xls(infile, outfile)
        elif out_format == "xml":
            self.csv_to_xml(infile, outfile)
        elif out_format == "xlsx":
            read_file = pd.read_csv(infile)
            resultExcelFile = pd.ExcelWriter(outfile)
            read_file.to_excel(resultExcelFile, index=False, header=True)
            resultExcelFile.close()
        else:
            return "Invalid output format for CSV", 404

        self._log.debug("convert done well.")

    def csv_to_json(self, csv_file, json_file):
        # SOURCE FROM https://stackoverflow.com/q/66543315
        self._log.debug("csv_to_json try to convert file: %s " % str(csv_file))
        json_array = []
        with open(csv_file, encoding='utf-8') as csv_file_stream:
            # load csv file data using csv library's dictionary reader
            csv_reader = csv.DictReader(csv_file_stream)

            # convert each csv row into python dict
            for row in csv_reader:
                # add this python dict to json array
                json_array.append(row)

        # convert python jsonArray to JSON String and write to file
        with open(json_file, 'w', encoding='utf-8') as jsonf:
            json_string = json.dumps(json_array, indent=4)
            jsonf.write(json_string)
        self._log.debug("csv_to_json done well.")

    def csv_to_xls(self, csv_file, xls_file):
        self._log.debug("csv_to_xls try to convert file: %s " % str(csv_file))
        read_file = pd.read_csv(csv_file)
        resultExcelFile = pd.ExcelWriter(xls_file)
        read_file.to_excel(resultExcelFile, index=False, header=True)
        resultExcelFile.close()

        self._log.debug("csv_to_xls done well.")

    def csv_to_xml(self, csv_file, xml_file):
        self._log.debug("csv_to_xml try to convert file: %s " % str(xml_file))
        csv_2_xml = Csv2Xml.Csv2Xml(csv_file, root='Optional', children='Optional')
        xml_string = csv_2_xml.parse2Xml()
        with open(xml_file, 'w', encoding='utf-8') as xmlf:
            xmlf.write(xml_string)
        self._log.debug("csv_to_xml done well.")

    def convert_codec2utf8(self, infile):
        self._log.debug("converting to uft-8")
        with open(infile, 'rb') as f:
            content_bytes = f.read()
        detected = chardet.detect(content_bytes)
        encoding = detected['encoding']
        self._log.debug(f"{infile}: detected as {encoding}.")
        if encoding != "utf-8":
            content_text = content_bytes.decode(encoding)
            file = codecs.open(infile, "w", "utf-8")
            file.write(content_text)
            file.close()
        else:
            self._log.debug("NO need to convert to uft-8")
