import os
import tempfile


# Defining a metaclass for AppConfiguration
class Singleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


class AppConfiguration(metaclass=Singleton):

    def __init__(self):
        self._logfile: str = None
        self._logging_level: str = None
        self._max_file_size: int = 457280

    def read_config(self):
        self._logfile = os.path.join(os.environ.get("LOGGING_FILE_PATH", tempfile.gettempdir()),
                                     os.environ.get("LOGGING_FILENAME", 'fifoc-app.log'))

        self._logging_level = os.environ.get('LOGGING_LEVEL', 'INFO').upper()
        self._max_file_size = os.environ.get('MAX_FILE_SIZE', 52428800)

    def get_logging_level(self) -> str:
        return self._logging_level

    def get_logfile(self) -> str:
        return self._logfile

    def get_max_file_size(self) -> int:
        return self._max_file_size

    logfile: str = property(get_logfile)
    logging_level: str = property(get_logging_level)
    max_file_size: int = property(get_max_file_size)
