echo "Running Development Server"
python -m ensurepip --default-pip
pip install virtualenv
virtualenv venv
pip install -r requirements.txt

export FLASK_APP=app.py
export FLASK_ENV=development
export FLASK_DEBUG=1
export PYTHONPATH=./src/

python tests\test_csv2any.py
python tests\test_html2any.py
python tests\test_ms_office2any.py
python tests\test_pdf2any.py
