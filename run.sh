echo "Running Development Server"
python -m ensurepip --default-pip
pip install virtualenv
virtualenv venv
pip install -r requirements.txt

export FLASK_APP=app.py
export FLASK_ENV=development
export FLASK_DEBUG=1
export PYTHONPATH=./src/
python -m flask run --port 9090 --debugger --with-threads --no-reload
