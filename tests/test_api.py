import pytest

from converters.csv_converter import (ACCEPTED_CSV_INPUT_FORMATS,
                                      ACCEPTED_CSV_OUTPUT_FORMATS)
from converters.docx_converter import (ACCEPTED_DOCX_INPUT_FORMATS,
                                       ACCEPTED_DOCX_OUTPUT_FORMATS)
from converters.excel_converter import (ACCEPTED_XLSX_INPUT_FORMATS,
                                        ACCEPTED_XLSX_OUTPUT_FORMATS)
from converters.html_converter import (ACCEPTED_HTML_INPUT_FORMATS,
                                       ACCEPTED_HTML_OUTPUT_FORMATS)
from converters.json_converter import (ACCEPTED_JSON_INPUT_FORMATS,
                                       ACCEPTED_JSON_OUTPUT_FORMATS)
from converters.pdf_converter import (ACCEPTED_PDF_INPUT_FORMATS,
                                      ACCEPTED_PDF_OUTPUT_FORMATS)
from app import create_app

format_collection_pairs = [(ACCEPTED_CSV_INPUT_FORMATS, ACCEPTED_CSV_OUTPUT_FORMATS),
                           (ACCEPTED_DOCX_INPUT_FORMATS, ACCEPTED_DOCX_OUTPUT_FORMATS),
                           (ACCEPTED_XLSX_INPUT_FORMATS, ACCEPTED_XLSX_OUTPUT_FORMATS),
                           (ACCEPTED_HTML_INPUT_FORMATS, ACCEPTED_HTML_OUTPUT_FORMATS),
                           (ACCEPTED_JSON_INPUT_FORMATS, ACCEPTED_JSON_OUTPUT_FORMATS),
                           (ACCEPTED_PDF_INPUT_FORMATS, ACCEPTED_PDF_OUTPUT_FORMATS)]
# PANDOC formats are excluded since it overlaps with other formats


@pytest.fixture()
def app():
    app = create_app()
    app.config.update({"TESTING": True})

    yield app


@pytest.fixture()
def client(app):
    return app.test_client()


@pytest.fixture()
def runner(app):
    return app.test_cli_runner()


def test_helper_endpoint(client):
    test_results = {}
    expected_results = {}
    for in_formats, out_formats in format_collection_pairs:
        for in_format in in_formats:
            expected_results[in_format] = f'Accepted Output Formats: {out_formats}'
    for in_formats, out_formats in format_collection_pairs:
        for in_format in in_formats:
            response = client.get(f"/v1/formats/{in_format}/")
            results = response.data.decode("utf-8")
            test_results[in_format] = results
    assert test_results == expected_results
