import os
import pathlib
import tempfile
import unittest


from converters.csv_converter import CsvConverter


class TestCasesCsv2Any(unittest.TestCase):

    def setUp(self):
        here = os.path.dirname(os.path.abspath(__file__))
        testdata_path = os.path.dirname(here)
        self.testdata_path = os.path.join(testdata_path, 'testdata', 'csv')

    def test_convert_csv2json_lr_pp(self):
        """Test converting a CSV to JSON"""
        out_format = 'json'
        infile = os.path.join(self.testdata_path, 'lr-pp-nov-2013.csv')
        tempdir = pathlib.Path(tempfile.mkdtemp())
        out = tempdir / "{}.{}".format("outfile", out_format)
        csv_converter = CsvConverter()
        csv_converter.convert(infile, out_format, out)
        self.assertEqual(os.path.getsize(out), 5141)  # add assertion here

    def test_convert_csv2json_simple(self):
        """Test converting a simple CSV to JSON"""
        out_format = 'json'
        infile = os.path.join(self.testdata_path, 'simple.csv')
        tempdir = pathlib.Path(tempfile.mkdtemp())
        out = tempdir / "{}.{}".format("outfile", out_format)
        csv_converter = CsvConverter()
        csv_converter.convert(infile, out_format, out)
        self.assertEqual(os.path.getsize(out), 622)  # add assertion here

    def test_convert_csv2json_empty_title(self):
        """Test converting a latin1 CSV to JSON"""
        out_format = 'json'
        infile = os.path.join(self.testdata_path, 'simple_empty_title.csv')
        tempdir = pathlib.Path(tempfile.mkdtemp())
        out = tempdir / "{}.{}".format("outfile", out_format)
        csv_converter = CsvConverter()
        csv_converter.convert(infile, out_format, out)
        self.assertEqual(os.path.getsize(out), 724)  # add assertion here

    def test_convert_csv2json_long(self):
        """Test converting a long CSV to JSON"""
        out_format = 'json'
        infile = os.path.join(self.testdata_path, 'long.csv')
        tempdir = pathlib.Path(tempfile.mkdtemp())
        out = tempdir / "{}.{}".format("outfile", out_format)
        csv_converter = CsvConverter()
        csv_converter.convert(infile, out_format, out)
        self.assertEqual(os.path.getsize(out), 160003)  # add assertion here

    def test_convert_csv2xml(self):
        """Test converting a long CSV to XML"""
        out_format = 'xml'
        infile = os.path.join(self.testdata_path, 'username.csv')
        tempdir = pathlib.Path(tempfile.mkdtemp())
        out = tempdir / "{}.{}".format("outfile", out_format)

        csv_converter = CsvConverter()
        csv_converter.convert(infile, out_format, out)
        self.assertEqual(os.path.getsize(out), 719)  # add assertion here


if __name__ == '__main__':
    unittest.main()
