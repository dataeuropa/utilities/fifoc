import os
import pathlib
import tempfile
import unittest
import pdfkit

from converters.pandoc_converter import PandocConverter

in_format = 'html'


class TestCasesHtml2Any(unittest.TestCase):

    def setUp(self):
        here = os.path.dirname(os.path.abspath(__file__))
        testdata_path = os.path.dirname(here)
        self.testdata_path = os.path.join(testdata_path, 'testdata', 'html')

        print(self.testdata_path)

    def test_convert_html2odt(self):
        """Test converting HTML to ODT"""
        out_format = 'odt'

        infile = os.path.join(self.testdata_path, 'simple.html')
        tempdir = pathlib.Path(tempfile.mkdtemp())
        out = tempdir / "{}.{}".format("outfile", out_format)
        pandoc_converter = PandocConverter()
        pandoc_converter.convert(infile, in_format, out_format, out)
        self.assertEqual(os.path.getsize(out), 9086)  # add assertion here

    def test_convert_html2docx(self):
        """Test converting HTML to DOCX"""
        out_format = 'docx'
        infile = os.path.join(self.testdata_path, 'simple.html')
        tempdir = pathlib.Path(tempfile.mkdtemp())
        out = tempdir / "{}.{}".format("outfile", out_format)
        pandoc_converter = PandocConverter()
        pandoc_converter.convert(infile, in_format, out_format, out)
        self.assertEqual(os.path.getsize(out), 9847)  # add assertion here

    def test_convert_html2pdf(self):
        """Test converting HTML to PDF"""
        out_format = 'pdf'
        infile = os.path.join(self.testdata_path, 'simple.html')
        tempdir = pathlib.Path(tempfile.mkdtemp())
        out = tempdir / "{}.{}".format("outfile", out_format)
        path_wkhtmltopdf = r'C:\Program Files\wkhtmltopdf\bin\wkhtmltopdf.exe'
        config = pdfkit.configuration(wkhtmltopdf=path_wkhtmltopdf)
        pdfkit.from_file(infile, out, configuration=config)
        # pdfkit.from_file(infile, out)
        # html_converter = HtmlConverter()
        # html_converter.convert(infile, out_format, out)
        print(out)
        print(os.path.getsize(out))
        self.assertEqual(os.path.getsize(out), 9487)  # add assertion here


if __name__ == '__main__':
    unittest.main()
