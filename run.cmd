python -m ensurepip --default-pip
pip install virtualenv
virtualenv venv
pip install -r requirements.txt

set FLASK_APP=app.py
set FLASK_ENV=development
set FLASK_DEBUG=1
set PYTHONPATH=./src/
python -m flask run --port 9090 --debugger --with-threads --no-reload
